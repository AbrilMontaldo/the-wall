<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;

class RegistrationType extends AbstractType
{
   public function buildForm(FormBuilderInterface $builder, array $options){
       $builder->add('nombre', null, ['required' => true]);
       $builder->add('apellido', null, ['required' => true]);
       $builder->add('profilePhoto', VichImageType::class, [
            'required' => false,
            'allow_delete' => true,
            'download_label' => 'Label',
            'download_uri' => true,
            'image_uri' => true,
        ]);
   }

   public function getParent(){
       return 'FOS\UserBundle\Form\Type\RegistrationFormType';
   }

   public function getBlockPrefix(){
       return 'app_user_registration';
   }

   public function getName(){
       return $this->getBlockPrefix();
   }
}
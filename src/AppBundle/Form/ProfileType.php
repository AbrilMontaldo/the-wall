<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProfileType extends AbstractType
{
   public function buildForm(FormBuilderInterface $builder, array $options){
       $builder->add('nombre', null, ['required' => true]);
       $builder->add('apellido', null, ['required' => true]);
       $builder->add('profilePhoto', VichImageType::class, [
            'required' => false,
            'allow_delete' => true,
            'download_label' => 'Label',
            'download_uri' => true,
            'image_uri' => true,
        ]);
   }

   public function getParent(){
       return 'FOS\UserBundle\Form\Type\ProfileFormType';
   }

   public function getBlockPrefix(){
       return 'app_user_profile';
   }

   public function getName(){
       return $this->getBlockPrefix();
   }
}
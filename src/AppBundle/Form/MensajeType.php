<?php
namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;

class MensajeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('contenido', TextareaType::class, ['label' => false]);
        $builder->add('photo', VichImageType::class, [
            'required' => false,
            'allow_delete' => true,
            'download_label' => 'Label',
        ]);
    }
}
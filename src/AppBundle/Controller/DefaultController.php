<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Form\MensajeType;
use AppBundle\Entity\Mensaje;
use AppBundle\Entity\User;

class DefaultController extends Controller
{
    public function __construct(){
    }
    
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $pagination = $this->getPaginatedMessages($request);
        $form = $this->createForm(MensajeType::class);

        return $this->render('index.html.twig', [
            'pagination' => $pagination,
            'form' => $form->createView()
        ]);
    }

    private function getPaginatedMessages($request){
        $entityManager = $this->getDoctrine()->getManager();
        // $expr = $entityManager->getExpressionBuilder();

        $followsId = $entityManager->createQueryBuilder()
            ->select('u2f.id')
            ->from('AppBundle:User', 'u2')
            ->join('u2.follows', 'u2f')
            ->where('u2.id = ?1')
            ->getQuery()
            ->setParameter(1, $this->getUser()->getId())
            ->execute();

        $qb = $entityManager->createQueryBuilder()
            ->select('m')
            ->from('AppBundle:Mensaje', 'm')
            ->join('m.user', 'u')
            ->where('u.id IN(:ids)')
            ->orderBy('m.created_at', 'DESC')
            ->setParameter('ids', $followsId);
        $query = $qb->getQuery(); //->execute();

        $paginator = $this->get('knp_paginator');
        return $paginator->paginate($query,$request->query->getInt('page', 1),10);
    }

    /**
     * @Route("/toggle-follow/{userId}", name="toggle-follow")
     */
    public function toggleFollowAction($userId){
        $manager = $this->getDoctrine()->getManager();
        $userLogueado = $this->getUser();
        $usuario = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($userId);
        if ($userLogueado->getFollows()->contains($usuario)){
            $userLogueado->removeFollow($usuario);
            $response = 'Seguir';
        }
        else{
            $userLogueado->addFollow($usuario);
            $response = 'Dejar de seguir';
        }
        $manager->persist($userLogueado);
        $manager->persist($usuario);
        $manager->flush();
        return new Response($response);
    }
    
    /**
     * @Route("/follow/{userId}", name="follow")
     */
    public function followAction($userId){
        $manager = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $usuarioASeguir = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($userId);
        $user->addFollow($usuarioASeguir);
        $manager->persist($user);
        $manager->persist($usuarioASeguir);
        $manager->flush();
        return new Response();
    }
    
    /**
     * @Route("/unfollow/{userId}", name="unfollow")
     */
    public function unfollowAction($userId){
        $manager = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $usuarioANoSeguir = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($userId);
        $user->removeFollow($usuarioANoSeguir);
        $manager->persist($user);
        $manager->persist($usuarioANoSeguir);
        $manager->flush();
        return new Response();
    }
    
    /**
     * @Route("/search", name="search")
     */
    public function searchAction(Request $request){
        $search = $request->query->get('text');
        $em = $this->getDoctrine()->getManager();

        $users = $em->createQuery(
                'SELECT u
                FROM AppBundle:User u
                WHERE u.nombre LIKE :search OR u.apellido LIKE :search OR u.username LIKE :search
                ORDER BY u.username ASC'
            )->setParameter('search', '%'.$search.'%')
            ->getResult();

        return $this->render('search.html.twig', [
            'usuarios' => $users,
            'search' => $search
        ]);
    }

    /**
     * @Route("/perfil/{id}", name="show-profile")
     */
    public function showProfileAction($id){
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);

        if (!$user) {
            throw $this->createNotFoundException(
                'No se encontró usuario con el id '.$id
            );
        }
    
        return $this->render('profile.html.twig', [
            'usuario' => $user
        ]);
    }

    public function mensajesPorFechaAction(Request $request){
        $pagination = $this->getMyPaginatedMessages($request);
        return $this->render('perfil/mensajes-paginados.twig', [
            'pagination' => $pagination
        ]);
    }

    public function mensajesLikeadosAction(Request $request){
        $entityManager = $this->getDoctrine()->getManager();
        $mensajes = $entityManager->createQueryBuilder()
            ->select('m')
            ->from('AppBundle:Mensaje', 'm')
            ->join('m.userLikes', 'mLikes')
            ->where('mLikes.id = ?1')
            ->getQuery()
            ->setParameter(1, $this->getUser()->getId())
            ->execute();
        
        return $this->render('perfil/mensajes.twig', [
            'mensajes' => $mensajes
        ]);
    }

    private function getMyPaginatedMessages($request){
        $entityManager = $this->getDoctrine()->getManager();
        $qb = $entityManager->createQueryBuilder()
            ->select('m')
            ->from('AppBundle:Mensaje', 'm')
            ->join('m.user', 'u')
            ->where('u.id = :user')
            ->orderBy('m.created_at', 'DESC')
            ->setParameter('user', $this->getUser()->getId());
        $query = $qb->getQuery();

        $paginator = $this->get('knp_paginator');
        return $paginator->paginate($query, $request->get('page') ?: 1, 10);
    }
}

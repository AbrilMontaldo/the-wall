<?php
namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AppBundle\Entity\Mensaje;
use AppBundle\Entity\User;

class ApiController extends FOSRestController
{   
    /**
     * @Route("/api/mensajes")
     */
    public function mensajesAction()
    {
        // $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        // if (!$auth) {
        //     throw new AccessDeniedException();
        // }
        // $user = $this->get('security.token_storage')->getToken()->getUser();
        
        $data = $this->getDoctrine()
            ->getRepository(Mensaje::class)
            ->findAll();
        $view = $this->view($data);
        $view->getContext()->setGroups(['list']);
        return $this->handleView($view);
    }

    /**
     * @Route("/api/usuarios/{id}")
     */
    public function userAction($id){
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($id);
        $view = $this->view($user);
        $view->getContext()->setGroups(['list', 'show']);
        return $this->handleView($view);
    }
}

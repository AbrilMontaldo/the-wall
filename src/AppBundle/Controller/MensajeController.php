<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Form\MensajeType;
use AppBundle\Entity\Mensaje;
use AppBundle\Entity\User;

class MensajeController extends Controller
{
    public function __construct(){
    }
    
    /**
     * @Route("mensajes/post", name="mensajes.post")
     */
    public function postAction(Request $request){
        $mensaje = new Mensaje();
        $form = $this->createForm(MensajeType::class, $mensaje);
        $form->handleRequest($request);
        if ($form->isSubmitted() & $form->isValid()) {
            $mensaje = $form->getData();
            $mensaje->setUser($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($mensaje);
            $entityManager->flush();
        }
        return $this->redirectToRoute('homepage');
    }
    
    /**
     * @Route("mensajes/delete/{mensajeId}", name="mensajes.delete")
     */
    public function deleteAction($mensajeId){
        $em = $this->getDoctrine()->getManager();
        $mensaje = $this->getDoctrine()
            ->getRepository(Mensaje::class)
            ->find($mensajeId);
        $em->remove($mensaje);
        $em->flush();
        return $this->redirectToRoute('fos_user_profile_show');
    }
    
    /**
     * @Route("/like/{mensajeId}", name="like")
     */
    public function toggleLikeAction($mensajeId){
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $mensaje = $this->getDoctrine()
            ->getRepository(Mensaje::class)
            ->find($mensajeId);
        if ($mensaje->getLikes()->contains($user)){
            $mensaje->removeLike($user);
            $classResponse = 'far fa-heart mr-1' ;
        }
        else{
            $mensaje->addLike($user);
            $classResponse = 'fas fa-heart mr-1' ;
        }
        $em->persist($mensaje);
        $em->persist($user);
        $em->flush();
        return new Response($classResponse);
    }
}

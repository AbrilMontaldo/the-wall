<?php
 namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixture extends Fixture implements OrderedFixtureInterface
{
    
    public function load(ObjectManager $manager)
    {
        $user = new User('abril@gmail.com', 'Abril', '123456');
        $user->setNombre('Abril');
        $user->setApellido('Montaldo');
        $user->setFoto('#');
        $manager->persist($user);
        $this->addReference('abril', $user);
        
        $user = new User('agustina@gmail.com', 'Agustina', '123456');
        $user->setNombre('Agustina');
        $user->setApellido('Olea');
        $user->setFoto('#');
        $manager->persist($user);
        $this->addReference('agustina', $user);
        
        $user = new User('leandro@gmail.com', 'Leandro', '123456');
        $user->setNombre('Leandro');
        $user->setApellido('Gomez');
        $user->setFoto('#');
        $manager->persist($user);
        $this->addReference('leandro', $user);

        $user = new User('romina@gmail.com', 'Romina', '123456');
        $user->setNombre('Romina');
        $user->setApellido('Peralta');
        $user->setFoto('#');
        $manager->persist($user);
        $this->addReference('romina', $user);

        $user = new User('axel@gmail.com', 'Axel', '123456');
        $user->setNombre('Axel');
        $user->setApellido('Covacich');
        $user->setFoto('#');
        $manager->persist($user);
        $this->addReference('axel', $user);
        
        $user = new User('camila@gmail.com', 'Camila', '123456');
        $user->setNombre('Camila');
        $user->setApellido('Ferreyra');
        $user->setFoto('#');
        $manager->persist($user);
        $this->addReference('camila', $user);

        $user = new User('ramiro@gmail.com', 'Ramiro', '123456');
        $user->setNombre('Ramiro');
        $user->setApellido('Cayuso');
        $user->setFoto('#');
        $manager->persist($user);
        $this->addReference('ramiro', $user);

        $user = new User('aldana@gmail.com', 'Aldana', '123456');
        $user->setNombre('Aldana');
        $user->setApellido('Roman');
        $user->setFoto('#');
        $manager->persist($user);
        $this->addReference('aldana', $user);
        
        $manager->flush();
    }

    public function getOrder(){
        return 1;
    } 
}
<?php
 namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Mensaje;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class MensajeFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $users = ['abril','agustina','leandro','romina','axel','camila','ramiro','aldana'];

        $randomStrs = [
            'Aenean et nulla lorem. Vestibulum in quam bibendum, scelerisque ex tempor, pharetra neque. Vivamus ac consequat nulla, nec scelerisque massa.',
            'Nam iaculis sem tortor, a mollis velit lobortis egestas. Sed pulvinar vulputate gravida. Maecenas quis aliquet enim, non consectetur diam.',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            'Etiam sed dapibus libero, a faucibus lectus. Donec nec lacus nec neque eleifend sodales.',
            'Donec et tempus mauris. Nunc et viverra dolor, id luctus tellus. Nulla vitae lobortis risus.'
        ];

        foreach ($users as $user) {
            $random = rand(1, 10);
            for ($i=1; $i < $random; $i++) { 
                $msj = new Mensaje($randomStrs[rand(0, 4)]);
                $msj->setUser($this->getReference($user));
                $manager->persist($msj);
            }    
        }

        $manager->flush();
    }

    public function getOrder(){
        return 2;
    }
}


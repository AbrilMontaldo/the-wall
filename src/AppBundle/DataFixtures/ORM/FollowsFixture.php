<?php
 namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class FollowsFixture extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $users = ['abril','agustina','leandro','romina','axel','camila','ramiro','aldana'];

        foreach ($users as $user) {
            $user = $this->getReference($user);
            $random = rand(0,4);
            for ($i=0; $i < $random; $i++) { 
                $userRandom = $this->getReference($users[rand(0, count($users)-1)]);
                if ($user != $userRandom){
                    $user->addFollow($userRandom);
                }
            }
            $manager->persist($user);
        }

        $manager->flush();
    }

    public function getOrder(){
        return 3;
    }
}


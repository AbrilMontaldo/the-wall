<?php 
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @Vich\Uploadable
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=144, nullable=true)
     */
    protected $nombre;

    /**
     * @ORM\Column(type="string", length=144, nullable=true)
     */
    protected $apellido;
    
    /**
     * @ORM\OneToMany(targetEntity="Mensaje", mappedBy="user", cascade={"persist"})
     */
    protected $mensajes;

    /**
     * @ORM\ManyToMany(targetEntity="Mensaje", mappedBy="userLikes")
     * @ORM\JoinTable(name="me_gustas",joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},inverseJoinColumns={@ORM\JoinColumn(name="mensaje_id", referencedColumnName="id")})
     * @ORM\OrderBy({"created_at" = "ASC"})
     */
    private $likes;
    
    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="follows")
    */
    private $followers;
    
    /**
     * @ORM\ManyToMany(targetEntity="User")
     * @ORM\JoinTable(name="follows")
     */
    private $follows;

    /**
     * @Vich\UploadableField(mapping="images", fileNameProperty="profilePhotoName", size="profilePhotoSize")
     * @var File
     */
    private $profilePhoto;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $profilePhotoName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var integer
     */
    private $profilePhotoSize;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct($email=null, $username=null, $password=null)
    {
        parent::__construct();
        $this->email = $email;
        $this->username = $username;
        $this->enabled = true;
        $this->setPlainPassword($password);
        $this->mensajes = new ArrayCollection();
        $this->likes = new ArrayCollection();
        $this->followers = new ArrayCollection();
        $this->follows = new ArrayCollection();
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context)
    {
        if(password_verify($this->getPlainPassword(),$this->getPassword())){
            $context->buildViolation('No podes usar la misma contrasenia')
                ->atPath('password')
                ->addViolation();
        }
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function getNombre(){
        return $this->nombre;
    }
    
    public function setFoto($foto){
        $this->foto = $foto;
    }

    public function getFoto(){
        return $this->foto;
    }
    
    public function setApellido($apellido){
        $this->apellido = $apellido;
    }

    public function getApellido(){
        return $this->apellido;
    }

    public function addMensaje(Mensaje $mensaje){
        $this->mensajes[] = $mensaje;
        $mensaje->setUser($this);
    }

    public function getMensajes(){
        return $this->mensajes;
    }

    public function addLike(Mensaje $mensaje){
        if (!$this->likes->contains($mensaje)){
            $this->likes->add($mensaje);
            $mensaje->addLike($this);
        }
    }

    public function removeLike(Mensaje $mensaje){
        if ($this->likes->contains($mensaje)){
            $this->likes->removeElement($mensaje);
            $mensaje->removeLike($this);
        }
    }

    public function getLikes(){
        return $this->likes;
    }

    public function addFollower(User $user){
        if (!$this->followers->contains($user)){
            $this->followers->add($user);
            $user->addFollow($this);
        }
    }

    public function addFollow(User $user){
        if (!$this->follows->contains($user)){
            $this->follows->add($user);
            $user->addFollower($this);
        }
    }

    public function removeFollower(User $user){
        if ($this->followers->contains($user)){
            $this->followers->removeElement($user);
            $user->removeFollow($this);
        }
    }

    public function removeFollow(User $user){
        if ($this->follows->contains($user)){
            $this->follows->removeElement($user);
            $user->removeFollower($this);
        }
    }

    public function getFollowers(){
        return $this->followers;
    }

    public function getFollows(){
        return $this->follows;
    }

    public function __toString(){
        return $this->nombre . ' ' . $this->apellido;
    }

    public function setProfilePhoto(?File $profilePhoto = null): void
    {
        $this->profilePhoto = $profilePhoto;

        if (null !== $profilePhoto) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getProfilePhoto(): ?File
    {
        return $this->profilePhoto;
    }

    public function setProfilePhotoName(?string $profilePhotoName): void
    {
        $this->profilePhotoName = $profilePhotoName;
    }

    public function getProfilePhotoName(): ?string
    {
        return $this->profilePhotoName;
    }
    
    public function setProfilePhotoSize(?int $profilePhotoSize): void
    {
        $this->profilePhotoSize = $profilePhotoSize;
    }

    public function getProfilePhotoSize(): ?int
    {
        return $this->profilePhotoSize;
    }

    public function isFollowing(User $user){
        return $this->follows->contains($user);
    }

    public function liked(Mensaje $mensaje){
        return $this->likes->contains($mensaje);
    }
}
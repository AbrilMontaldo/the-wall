<?php 
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @ORM\Table(name="mensajes")
 * @Vich\Uploadable
 */
class Mensaje
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=144)
     */
    protected $contenido;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $created_at;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="mensajes")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id",nullable=false)
     */    
    protected $user;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="likes")
    */
    protected $userLikes;

    /**
     * @Vich\UploadableField(mapping="images", fileNameProperty="photoName", size="photoSize")
     * @var File
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photoName;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @var integer
     */
    private $photoSize;

    public function __construct($contenido = null)
    {
        if ($contenido){
            $this->contenido = $contenido;
        }
        $this->userLikes = new ArrayCollection();
        $this->created_at = new \DateTime();
    }

    public function getId(){
        return $this->id;
    }

    public function setContenido($contenido){
        $this->contenido = $contenido;
    }

    public function getContenido(){
        return $this->contenido;
    }
    
    public function setUser($user){
        $this->user = $user;
    }

    public function getUser(){
        return $this->user;
    }

    public function getFecha(){
        return $this->created_at;   
    }
    
    public function setFecha($fecha){
        $this->created_at = $fecha;
    }

    public function addLike(User $user){
        if (!$this->userLikes->contains($user)){
            $this->userLikes->add($user);
            $user->addLike($this);   
        }
    }

    public function removeLike(User $user){
        if ($this->userLikes->contains($user)){
            $this->userLikes->removeElement($user);
            $user->removeLike($this);
        }
    }

    public function getLikes(){
        return $this->userLikes;
    }

    public function setPhoto(?File $photo = null): void
    {
        $this->photo = $photo;

        if (null !== $photo) {
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getPhoto(): ?File
    {
        return $this->photo;
    }

    public function setPhotoName(?string $photoName): void
    {
        $this->photoName = $photoName;
    }

    public function getPhotoName(): ?string
    {
        return $this->photoName;
    }
    
    public function setPhotoSize(?int $photoSize): void
    {
        $this->photoSize = $photoSize;
    }

    public function getPhotoSize(): ?int
    {
        return $this->photoSize;
    }

    public function getLikesCount(){
        return $this->userLikes->count();
    }
}